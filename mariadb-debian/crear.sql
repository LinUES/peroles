use mysql ;
flush privileges ;
grant all privileges on *.* to 'root'@'%'         identified by 'ues' with grant option ;
grant all privileges on *.* to 'root'@'localhost' identified by 'ues' with grant option ;
set password for 'root'@'%'         = password('ues') ;
set password for 'root'@'localhost' = password('ues') ;
drop database if exists test ;
flush privileges ;
grant all privileges on *.* to 'tizon'@'%'         identified by 'ues' with grant option ;
grant all privileges on *.* to 'tizon'@'localhost' identified by 'ues' with grant option ;
set password for 'tizon'@'%'         = password('ues') ;
set password for 'tizon'@'localhost' = password('ues') ;
create database cumbo ;
grant all privileges on cumbo.* to 'tizon'@'%'         ;
grant all privileges on cumbo.* to 'tizon'@'localhost' ;
flush privileges ;
use cumbo;
create table usuario(
  id_usuario     integer not null auto_increment primary key ,
  nombre_usuario text    not null                            ,
  clave_usuario  text    not null                            
);
