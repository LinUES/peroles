# mariadb-alpine

La idea era tener un contenedor, que corra mariadb y posea una base de datos precreada que se pueda acceder desde otra aplicación o contenedor adicional.

## Como construirla

Suponiendo que ya tienen Docker instalado, y se ha descargado este perol se podrá construir con :

    cd mariadb-alpine
    docker build -t mariadb-alpine .

Espero todo salga bien, la idea es no achicoplarse.

## Para ejecutarlo

Si no se tiene ocupado el puerto **3306** (con otra posible instancia de mariadb)

    docker run -p 3306:3306 -d mariadb-alpine

En caso de tener ocupado el puerto se puede ir por otro adicional como el **3307**, sólo porque sí :

    docker run -p 3307:3306 -d mariadb-alpine.

## Para usarlo

En el archivo [crear.sql](./crear.sql) podrán darse cuenta del (desastre y mal) intento de la contraseña para **root** y para un usuario llamado **tizon**, y una base de datos llamada **cumbo**, en resumen :

| usuario | clave |
|---------|-------|
| root    | ues   |
| tizon   | ues   |

    mariadb -P 3306 -u root -pues
    mariadb -P 3306 -u tizon -pues cumbo

## Por hacer

Ni idea por el momento; pero ¡ahí tá!
