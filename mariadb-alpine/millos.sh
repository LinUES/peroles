echo "#------ Cambiar persmisos de usuario mysql ------ #"
chown -R mysql:mysql /var/lib/mysql
mysql_install_db --user=mysql --ldata=/var/lib/mysql

mariadbd-safe --datadir='/var/lib/mysql' &
PID="$!"
sleep 3s

echo "#------ Crear usuarios, base y tabla ------ #"
/usr/bin/mariadb < /tmp/crear.sql
kill -9 $PID
rm -f /tmp/*
